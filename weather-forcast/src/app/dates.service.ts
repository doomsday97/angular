import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DatesService {
 

  weather : any;

 httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getDate(): Observable<any> {
    return this.http.get<any>('./assets/dataset.json');
  }

  setCurrentWeather(weather)
  {
    this.weather = weather;
  }
  getCurrentWeather()
  {
    return this.weather;
  }
}
