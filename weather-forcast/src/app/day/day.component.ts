import { Component, OnInit } from '@angular/core';
import { DatesService } from '../dates.service';
import {Router } from "@angular/router";



@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit {

  data : any;
  weather: any;
 
  constructor( private DateService : DatesService, private router : Router) { }
  ngOnInit() {
    this.DateService.getDate().subscribe(response =>{
         console.log(response);
         this.data = response.list;
         this.weather = response.list;
    })
         
  }
  onClick(e) {
    this.DateService.setCurrentWeather(e);
    console.log(this.DateService.getCurrentWeather());
    this.router.navigateByUrl("/details");
  }
}
