import { Component, OnInit } from '@angular/core';
import {DatesService} from '../dates.service'
import {Router } from "@angular/router";


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  weather: any;
  constructor(public service: DatesService,private router : Router) { }

  ngOnInit() {

    this.weather = this.service.getCurrentWeather();
  }


  onclick()
  {
    this.router.navigateByUrl("/day");
  }

}
