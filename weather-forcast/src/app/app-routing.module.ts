import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DayComponent} from "./day/day.component";
import { DetailsComponent } from './details/details.component';

const routes: Routes = [

  { path: '', component: DayComponent , pathMatch: 'full' },
  { path:'day', component: DayComponent},
  {path:'details', component: DetailsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents =[DetailsComponent,DayComponent];